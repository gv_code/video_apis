'use strict'

module.exports = {
    name: String,
    description: String,
    quality: String,
    imageUrl: {
        url: String,
        thumbnail: String,
        resize_url: String,
        resize_thumbnail: String
    },
    liveUrl: {
        url: String,
    },

    language: {
        type: String,
        default: 'english/hindi',
        enum: ['english', 'hindi', 'english/hindi']
    }

}