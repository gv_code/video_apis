"use strict";

module.exports = {
  name: {
    type: String,
    lowercase: true,
    trim: true,
  },
  image: {
    url: String,
    thumbnail: String,
    resize_url: String,
    resize_thumbnail: String,
  },
  description: {
    type: String,
    lowercase: true,
  },
  isWebSeries: {
    type: Number,
    default: 1,
    enum: [0, 1],
  },
};
