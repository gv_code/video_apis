"use strict";

module.exports = {
  name: { type: String, lowercase: true, trim: true },
  description: String,
  category: { type: String, lowercase: true, trim: true },
  industry: String,
  release: String,
  runtime: String,
  quality: String,
  imageUrl: [
    {
      url: String,
      thumbnail: String,
      resize_url: String,
      resize_thumbnail: String,
    },
  ],
  movieUrl: [
    {
      url: String,
      url2: String,
    },
  ],
  isRecommanded: {
    type: String,
    default: "false",
    enum: ["true", "false"],
  },
  language: {
    type: String,
    default: "english/hindi",
    enum: ["english", "hindi", "english/hindi"],
  },
  movies: [],
  series: [],
};
