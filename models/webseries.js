"use strict";

module.exports = {
  movieName: {
    type: String,
    lowercase: true,
  },
  ImageUrlHorizontal: {
    type: String,
  },
  ImageUrlVertical: {
    type: String,
  },
  videoUrl: {
    type: String,
  },
  videoUrlSecond: {
    type: String,
  },
  downloadUrlOne: {
    type: String,
  },
  downloadUrlSecond: {
    type: String,
  },
  rating: {
    type: String,
  },
  catergory: {
    type: String,
  },

  latest: {
    type: String,
  },
  keyName: {
    type: String,
    lowercase: true,
  },
  pathName: {
    type: String,
  },
  Industry: {
    type: String,
  },
};
