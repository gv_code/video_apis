"use strict";

// Outlet Module
module.exports = {
  parent_id: String,
  name: {
    type: String,
    lowercase: true,
    trim: true,
  },
  image: {
    url: String,
    thumbnail: String,
    resize_url: String,
    resize_thumbnail: String,
  },
  runtime: String,
  quality: String,
  description: String,
  release: String,
  videoUrl: String,
  videoUrlSecond: String,
  downloadUrl: String,
  downloadUrlSecond: String,
  rating: String,
};
