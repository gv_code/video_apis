'use strict'

module.exports = {
    title: String,
    dislike: String,
    like: String,
    duration: String,
    img: String,
    quality: String,
    playerUrl: String,
    url: String,
    description: String,
    tags: {
        default: [],
        type: []
    },
    models: {
        default: [],
        type: []
    },
    categories: {
        default: [],
        type: []
    },
    downloadData: {
        default: [],
        type: []
    },
    createdAt: {
        type: Date,
        default: new Date()
    }

}