'use strict'

module.exports = {
    title: String,
    url: String,
    like: String,
    duration: String,
    description: String,
    image: String,
    views:String,
    videoWidth:String,
    videoHeight:String,
    files:{
        low:String,
        high:String,
        HLS:String,
        thumb:String,
        thumb69:String,
        thumbSlide:String,
        thumbSlideBig:String,
    },   
    quality: String,
    channel: String,
    url: String,
    tags: [],


}