'use strict'

module.exports = {
    title: String,
    message: String,
    version: String,
    link: String,
    isImportant: {
        type: String,
        default: 'false',
        enum: ['true', 'false']
    },
    forApproval: {
        type: String,
        default: 'false',
        enum: ['true', 'false']
    },
    forApproval1: {
        type: String,
        default: 'false',
        enum: ['true', 'false']
    }

}