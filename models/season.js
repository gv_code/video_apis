'use strict'

// Outlet Module
module.exports = {
   
    name: {
        type: String,
        lowercase: true,
        trim: true
    },
    parent_id:String
}