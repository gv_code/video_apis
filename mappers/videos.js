exports.toModel = entity => {

    var model = {
        _id: entity._id,
        title: entity.title,
        dislike: entity.dislike,
        like: entity.like,
        duration: entity.duration,
        img: entity.img,
        quality: entity.quality,
    }
    return model
}
