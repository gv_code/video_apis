"use strict";

const response = require("../exchange/response");

exports.canCreate = (req, res, next) => {
  if (!req.body.name) {
    response.failure(res, "name_required");
  }

  return next();
};
