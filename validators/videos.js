'use strict'

const response = require('../exchange/response')

exports.create = (req, res, next) => {
    if (!req.body.name) {
        return response.failure(res, 'Name is required')
    }
    if (!req.body.description) {
        return response.failure(res, 'question is required')
    }
    // if (!req.body.options) {
    //     return response.failure(res, 'options is required')
    // }
    // if (!req.body.answer) {
    //     return response.failure(res, 'answer is required')
    // }
    return next()
}

exports.getById = (req, res, next) => {

    if (!req.params && !req.params.id) {

        return response.failure(res, 'id is required')
    }

    return next()
}