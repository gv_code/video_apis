'use strict'

const response = require('../exchange/response')

exports.create = (req, res, next) => {
    if (!req.body.videoId) {
        return response.failure(res, 'videoId is required')
    }
    if (!req.body.type) {
        return response.failure(res, 'type is required')
    }
    // if (!req.body.version) {
    //     return response.failure(res, 'version is required')
    // }
    // if (!req.body.isImportant) {
    //     return response.failure(res, 'isImportant is required')
    // }
    return next()
}

exports.getById = (req, res, next) => {

    if (!req.params && !req.params.id) {

        return response.failure(res, 'id is required')
    }

    return next()
}