'use strict'

const response = require('../exchange/response')

exports.canCreate = (req, res, next) => {
    if (!req.body.name) {
        response.failure(res, 'Name is required')
    }
    if (!req.body.parent_id) {
        response.failure(res, 'parent_id is required')
    }
    return next()
}