'use strict'

const response = require('../exchange/response')
const service = require('../services/update')
const mapper = require('../mappers/update')

exports.create = async (req, res) => {
    const log = req.context.logger.start(`api/update/create`)
    try {
       
        
        
        const movies = await service.create(req.body, req.context)
        log.end()
        return response.data(res, movies)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }

}

exports.getById = async (req, res) => {
    const log = req.context.logger.start(`api/update/get/${req.params.id}`)

    try {
        const movies = await service.getById(req.params.id, req.context)
        log.end()
        return response.data(res, movies)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}
exports.get = async (req, res) => {
    const log = req.context.logger.start(`api/update/get`)

    try {
        const movies = await service.get(req, req.context)
        log.end()
        return response.data(res, movies)

    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}