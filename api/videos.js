"use strict";

const response = require("../exchange/response");
const service = require("../services/videos");
const mapper = require("../mappers/movies");
const pagination = require("../helpers/paging");

exports.create = async (req, res) => {
  const log = req.context.logger.start(`api/videos/create`);
  try {
    const movies = await service.create(req.body, req.context);
    log.end();
    return response.data(res, movies);
  } catch (err) {
    log.error(err.message);
    log.end();
    return response.failure(res, err.message);
  }
};

exports.getById = async (req, res) => {
  const log = req.context.logger.start(`api/videos/get/${req.params.id}`);

  try {
    const movies = await service.getById(req.params.id, req.context);
    log.end();
    return response.data(res, movies);
  } catch (err) {
    log.error(err.message);
    log.end();
    return response.failure(res, err.message);
  }
};
exports.get = async (req, res) => {
  const log = req.context.logger.start(`api/videos/get`);

  try {
    const page = pagination.pager(req);
    const movies = await service.get(req,page, req.context);
    log.end();
    return response.data(res, movies);
  } catch (err) {
    log.error(err.message);
    log.end();
    return response.failure(res, err.message);
  }
};

exports.deleteById = async (req, res) => {
  const log = req.context.logger.start(`api/videos/delete${req.params.id}`);
  try {
    const movies = await service.deleteById(req.params.id, req.context);
    log.end();
    return response.data(res, "successfully removed");
  } catch (err) {
    log.error(err.message);
    log.end();
    return response.failure(res, err.message);
  }
};

exports.deleteAll = async (req, res) => {
  const log = req.context.logger.start(`api/videos/delete`);
  try {
    const movies = await service.deleteAll(req.context, res);
    log.end();
    return response.data(res, "successfully removed");
  } catch (err) {
    log.error(err.message);
    log.end();
    return response.failure(res, err.message);
  }
};

exports.search = async (req, res) => {
  const log = req.context.logger.start(`api/videos/search`);

  try {
    const page = pagination.pager(req);
    const movies = await service.search(req, page, req.context);
    log.end();
    return response.data(res, movies);
  } catch (err) {
    log.error(err.message);
    log.end();
    return response.failure(res, err.message);
  }
};

exports.getVideoThumb = async (req, res) => {
  const log = req.context.logger.start(`api/videos/getvideothumb`);

  try {
    const movies = await service.getVideoThumb(req, req.context);
    log.end();
    return response.direct(res, movies);

  } catch (err) {
    log.error(err.message);
    log.end();
    return response.failure(res, err.message);
  }
};
