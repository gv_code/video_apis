'use strict'

const response = require('../exchange/response')
const service = require('../services/category')
// const mapper = require('../mappers/category')

exports.create = async (req, res) => {
    const log = req.context.logger.start(`api/category/create`)
    try {
       
        
        
        const livetv = await service.create(req.body, req.context)
        log.end()
        return response.data(res, livetv)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }

}

exports.getById = async (req, res) => {
    const log = req.context.logger.start(`api/category/get/${req.params.id}`)

    try {
        const category = await service.getById(req.params.id, req.context)
        log.end()
        return response.data(res, category)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}
exports.get = async (req, res) => {
    const log = req.context.logger.start(`api/category/get`)

    try {
        const category = await service.get(req, req.context)
        log.end()
        return response.data(res, category)

    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}