'use strict'

const response = require('../exchange/response')
const service = require('../services/webSeries')

exports.create = async (req, res) => {
    const log = req.context.logger.start(`api/webSeries`)
    try {
        const category = await service.create(req.body, req.context)
        log.end()
        return response.data(res, category) 

    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}
exports.createMovie = async (req, res) => {
    const log = req.context.logger.start(`api/createMovie`)
    try {
        // const category = await service.createMovie(req.body, req.context)
        // log.end()
        // return response.data(res, category) 

    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}
exports.get = async (req, res) => {
    const log = req.context.logger.start(`api/webSeries/get`)
    try {
        const webSeries = await service.get(req, req.context)
        log.end()
        return response.data(res, webSeries)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res.err.message)
    }
}