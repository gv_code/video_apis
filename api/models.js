'use strict'

const response = require('../exchange/response')
const service = require('../services/models')
// const mapper = require('../mappers/models')
const pagination = require("../helpers/paging");

exports.create = async (req, res) => {
    const log = req.context.logger.start(`api/models/create`)
    try {
       
        
        
        const livetv = await service.create(req.body, req.context)
        log.end()
        return response.data(res, livetv)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }

}

exports.getById = async (req, res) => {
    const log = req.context.logger.start(`api/models/get/${req.params.id}`)

    try {
        const models = await service.getById(req.params.id, req.context)
        log.end()
        return response.data(res, models)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}
exports.get = async (req, res) => {
    const log = req.context.logger.start(`api/models/get`)

    try {
    const page = pagination.pager(req);

        const models = await service.get(req, page, req.context)
        log.end()
        return response.data(res, models)

    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}