'use strict'

const response = require('../exchange/response')
const service = require('../services/zvideos')
// const mapper = require('../mappers/update')

exports.create = async (req, res) => {
    const log = req.context.logger.start(`api/update/create`)
    try {

        const movies = await service.create(req.body, req.context)
        log.end()
        return response.data(res, movies)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }

}
exports.getById = async (req, res) => {
    const log = req.context.logger.start(`api/test/get/${req.params.id}`)

    try {
        const video = await service.getById(req.params.id, req.context)
        log.end()
        return response.data(res, video)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}



exports.get = async (req, res) => {
    const log = req.context.logger.start(`api/zvideos/get`)
    try {
        const videos = await service.get(req, req.context)
        log.end()
        return response.data(res, videos)

    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

exports.start = async (req, res) => {
    const log = req.context.logger.start(`api/zvideos/start`)
    try {
        const videos = await service.start(req, req.context)
        log.end()
        return response.data(res, videos)

    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}






