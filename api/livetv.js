'use strict'

const response = require('../exchange/response')
const service = require('../services/livetv')
const mapper = require('../mappers/livetv')

exports.create = async (req, res) => {
    const log = req.context.logger.start(`api/livetv/create`)
    try {
       
        
        
        const livetv = await service.create(req.body, req.context)
        log.end()
        return response.data(res, livetv)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }

}

exports.getById = async (req, res) => {
    const log = req.context.logger.start(`api/livetv/get/${req.params.id}`)

    try {
        const livetv = await service.getById(req.params.id, req.context)
        log.end()
        return response.data(res, livetv)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}
exports.get = async (req, res) => {
    const log = req.context.logger.start(`api/livetv/get`)

    try {
        const livetv = await service.get(req, req.context)
        log.end()
        return response.data(res, livetv)

    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}