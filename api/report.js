'use strict'

const response = require('../exchange/response')
const service = require('../services/report')
const mapper = require('../mappers/report')

exports.create = async (req, res) => {
    const log = req.context.logger.start(`api/report`)

    try {
        const report = await service.create(req.body, req.context)
        log.end()
        return response.data(res, report)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }

}



exports.getById = async (req, res) => {
    const log = req.context.logger.start(`api/report/getById/${req.params.id}`)

    try {
        const user = await service.getById(req.params.id, req.context)
        log.end()
        return response.data(res, mapper.toGetUser(user))
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

exports.get = async (req, res) => {
    const log = req.context.logger.start(`api/report/get`)

    try {
        const user = await service.get(req.context)
        log.end()
        return response.data(res, user)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}


exports.delete = async (req, res) => {
    const log = req.context.logger.start(`api/report/delete${req.params.id}`)
    try {
        const category = await service.deleteReport(req.params.id, req.context)
        log.end()
        return response.data(res, 'successfully removed')
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}




