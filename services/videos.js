"use strict";
const testService = require("./test")
const mapper = require("../mappers/videos");
const createTempModel = async (movies, series) => {
  var result = {
    movies: movies,
    series: series,
  };
  return result;
};

const create = async (model, context) => {
  const log = context.logger.start("services/videos/create");
  try {
    let movies = await new db.videos(model).save();
    log.end();
    return movies;
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};

const getById = async (id, context) => {
  const log = context.logger.start(`services/videos/get:${id}`);

  try {
    var movies = await db.videos.findById(id);
    var relatedVideos = [];
    if (movies) {
      let videoData = await testService.getByIdFromOther(movies.url, context)
      if (videoData) {
        // movies.description = videoData.description;
        // movies.models = videoData.models;
        // movies.categories = videoData.category;
        // movies.tags = videoData.tags
        // movies.playerUrl = videoData.playerUrl
        // movies.playerImg = videoData.playerImage
        movies.downloadData = videoData.downloadData
        movies.like = videoData.like
        movies.dislike = videoData.dislike
        // movies.createdAt= new Date();
        movies.save();

        relatedVideos = await getRelatedVideo(movies.models, id)
        relatedVideos = relatedVideos.filter((item, pos) => {
          return relatedVideos.indexOf(item) == pos;
        })

      } else {
        throw new Error("video data not found");

      }
    } else {
      throw new Error("video not found");

    }

    log.end();
    return { video: movies, relatedVideos: relatedVideos };
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};


const getByIdForAuto = async (id, context) => {
  const log = context.logger.start(`services/videos/get:${id}`);

  try {
    var movies = await db.videos.findById(id);
    var relatedVideos = [];
    if (movies) {
      let videoData = await testService.getById(movies.url, context)
      if (videoData) {
        movies.description = videoData.description;
        movies.models = videoData.models;
        movies.categories = videoData.category;
        movies.tags = videoData.tags
        movies.playerUrl = videoData.playerImage
        // movies.downloadData = videoData.downloadData
        movies.createdAt = new Date();
        movies.save();

        // relatedVideos = await getRelatedVideo(movies.models,id)
        // relatedVideos = relatedVideos.filter((item, pos) => {
        //   return relatedVideos.indexOf(item) == pos;
        // })

      } else {
        throw new Error("video data not found");

      }
    } else {
      throw new Error("video not found");

    }

    log.end();
    return { video: movies, relatedVideos: relatedVideos };
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};

const get = async (req, page, context) => {
  const log = context.logger.start(`api/videos/get`);
  let count;
  try {

    let movies;
    let q = req.query;
    if (q.category) {
      movies = await db.videos.find({ categories: { $elemMatch: { title: { $regex: q.category } } } }).skip(page.skipCount)
        .limit(page.items)
        .sort({
          createdAt: -1,
        });
      count = await db.videos.find({ categories: { $elemMatch: { title: { $regex: q.category } } } })
      count = count ? count.length : 0;

    } else if (q.model) {
      movies = await db.videos.find({ models: { $elemMatch: { title: { $regex: q.model } } } }).skip(page.skipCount)
        .limit(page.items)
        .sort({
          createdAt: -1,
        });
      count = await db.videos.find({ models: { $elemMatch: { title: { $regex: q.model } } } })
      count = count ? count.length : 0;

    } else if (q.tag) {
      movies = await db.videos.find({ tags: { $elemMatch: { $regex: q.tag } } }).skip(page.skipCount)
        .limit(page.items)
        .sort({
          createdAt: -1,
        });
      count = await db.videos.find({ tags: { $elemMatch: { $regex: q.tag } } });
      count = count ? count.length : 0;

    } else if(q.top) {
      movies = await db.videos.find({}).sort({
        like:-1,
        createdAt: -1,
      }).skip(page.skipCount).limit(page.items);
      count = await db.videos.count()
    } else {
      movies = await db.videos.find({}).sort({
        createdAt: -1,
      }).skip(page.skipCount).limit(page.items);
      count = await db.videos.count()
    }

    // let category = await db.category.find({})

    let totalCount = count;
    let totalPages = totalCount / page.items;
    var p = totalPages.toString().split('.');
    totalPages = Math.floor(totalPages);
    if (p[1]) {
      totalPages++;
    };
    movies = movies.map((m)=>mapper.toModel(m))
    var result = {
      movies: movies,
      totalCount: totalCount,
      totalPages: totalPages,
      // category: category
    };

    // let createUserTempModel = await userTempModel(movies, totalPages, totalCount, category);
    // movies = createUserTempModel;
    log.end();
    // return movies;
    return result;
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};
const userTempModel = async (movies, totalPages, totalCount, category) => {
  var result = {
    movies: movies,
    totalCount: totalCount,
    totalPages: totalPages,
    category: category
  };
  return result;
};
const search = async (req, page, context) => {
  const log = context.logger.start(`api/movies/search`);
  try {
    let movies;
    let webseries;
    let params = req.query;
    let query = {};
    if (params) {

      // search by name
      if (params.name) {
        query = {
          name: { $regex: params.name.toLowerCase() },
        };
        movies = await db.movies.find(query).sort({
          timeStamp: -1,
        });
        webseries = await db.series.find(query).sort({
          timeStamp: -1,
        });
        const searchResult = await createTempModel(movies, webseries);
        movies = searchResult;
      }

      // search according to categories
      if (params.category && params.category == "true") {
        movies = await db.movies.aggregate([
          { $group: { _id: "$category", movies: { $push: "$$ROOT" } } },
          {
            $project: {
              movies: { $slice: ["$movies", 10] },
            },
          },
        ]);
      }

      // search for particular category
      if (params.categoryName) {
        query = {
          category: params.categoryName,
        };
        if (page != null && page != undefined && page != "") {
          movies = await db.movies
            .find(query)
            .skip(page.skipCount)
            .limit(page.items)
            .sort({
              timeStamp: -1,
            });
        } else {
          movies = await db.movies.find(query).sort({
            timeStamp: -1,
          });
        }
      }

      // search all movies with paging
      if (
        (params.name == null ||
          params.name == undefined ||
          params.name == "") &&
        (params.category == null ||
          params.category == undefined ||
          params.category == "") &&
        (params.categoryName == null ||
          params.categoryName == undefined ||
          params.categoryName == "")
      ) {
        if (page != null && page != undefined && page != "") {
          query = {};
          movies = await db.movies
            .find(query)
            .skip(page.skipCount)
            .limit(page.items)
            .sort({
              timeStamp: -1,
            });
        }
      }
    } else {
      movies = await db.movies.find(query).sort({
        timeStamp: -1,
      });
    }
    log.end();
    return movies;
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};

const deleteById = async (id, context, res) => {
  const log = context.logger.start(`services/movie/delete:${id}`);
  try {
    const movies = await db.movies.findByIdAndRemove(id);

    log.end();
    return movies;
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};

const deleteAll = async (context, res) => {
  const log = context.logger.start(`services/movie/delete`);
  try {
    const movies = await db.videos.remove();

    log.end();
    return movies;
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};

const getRelatedVideo = async (models, id) => {
  return new Promise((resolve, reject) => {
    const relatedVideos = [];
    models.forEach(async (m, i) => {
      const movies = await db.videos.find({ models: { $elemMatch: { title: { $regex: m.title } } } })
        .limit(9)
        .sort({
          createdAt: -1,
        });
      if (relatedVideos.length < 9) {
        movies.forEach((v) => {
          if (relatedVideos.length < 9 && v._id != id) {
            relatedVideos.push(v);
          }
        })
      }
      if (relatedVideos.length > 8 || i == models.length - 1) {
        resolve(relatedVideos)

      }

    });
  })
}

const getVideoThumb = async (req, context) => {
  const log = context.logger.start(`services/videos/getvideothumb`);
  let q = req.query;
  try {

      let time = await testService.getVideoThumb(q.sec,q.id, context)
      


    log.end();
    return time;
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};

exports.create = create;
exports.getById = getById;
exports.getByIdForAuto = getByIdForAuto;
exports.get = get;
exports.deleteById = deleteById;
exports.deleteAll = deleteAll;
exports.search = search;
exports.getVideoThumb = getVideoThumb
