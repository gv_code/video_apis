"use strict";
const createTempModel = async (movies, series) => {
  var result = {
    movies: movies,
    series: series,
  };
  return result;
};

const create = async (model, context) => {
  const log = context.logger.start("services/movies/create");
  try {
    let movies = await new db.movies(model).save();
    log.end();
    return movies;
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};

const getById = async (id, context) => {
  const log = context.logger.start(`services/movies/get:${id}`);

  try {
    const movies = await db.movies.findById(id);
    log.end();
    return movies;
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};

const get = async (req, context) => {
  const log = context.logger.start(`api/movies/get`);
  try {
    let movies;
    movies = await db.movies.find({}).sort({
      timeStamp: -1,
    });

    log.end();
    return movies;
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};

const search = async (req, page, context) => {
  const log = context.logger.start(`api/movies/search`);
  try {
    let movies;
    let webseries;
    let params = req.query;
    let query = {};
    if (params) {
      
      // search by name
      if (params.name) {
        query = {
          name: { $regex: params.name.toLowerCase() },
        };
        movies = await db.movies.find(query).sort({
          timeStamp: -1,
        });
        webseries = await db.series.find(query).sort({
          timeStamp: -1,
        });
        const searchResult = await createTempModel(movies, webseries);
        movies = searchResult;
      }

      // search according to categories
      if (params.category && params.category == "true") {
        movies = await db.movies.aggregate([
          { $group: { _id: "$category", movies: { $push: "$$ROOT" } } },
          {
            $project: {
              movies: { $slice: ["$movies", 10] },
            },
          },
        ]);
      }

      // search for particular category
      if (params.categoryName) {
        query = {
          category: params.categoryName,
        };
        if (page != null && page != undefined && page != "") {
          movies = await db.movies
            .find(query)
            .skip(page.skipCount)
            .limit(page.items)
            .sort({
              timeStamp: -1,
            });
        } else {
          movies = await db.movies.find(query).sort({
            timeStamp: -1,
          });
        }
      }

      // search all movies with paging
      if (
        (params.name == null ||
          params.name == undefined ||
          params.name == "") &&
        (params.category == null ||
          params.category == undefined ||
          params.category == "") &&
        (params.categoryName == null ||
          params.categoryName == undefined ||
          params.categoryName == "")
      ) {
        if (page != null && page != undefined && page != "") {
          query = {};
          movies = await db.movies
            .find(query)
            .skip(page.skipCount)
            .limit(page.items)
            .sort({
              timeStamp: -1,
            });
        }
      }
    } else {
      movies = await db.movies.find(query).sort({
        timeStamp: -1,
      });
    }
    log.end();
    return movies;
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};

const deleteById = async (id, context, res) => {
  const log = context.logger.start(`services/movie/delete:${id}`);
  try {
    const movies = await db.movies.findByIdAndRemove(id);

    log.end();
    return movies;
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};

const deleteAll = async (context, res) => {
  const log = context.logger.start(`services/movie/delete`);
  try {
    const movies = await db.movies.remove();

    log.end();
    return movies;
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};

exports.create = create;
exports.getById = getById;
exports.get = get;
exports.deleteById = deleteById;
exports.deleteAll = deleteAll;
exports.search = search;
