"use strict";
const seasonService = require("./season");
const episodeService = require("./episode");
const service = require("./webSeries");
const e = require("express");


const create = async () => {
  // const log = context.logger.start(`services/series`);
  try {
    const a = await db.series.remove();
    const b = await db.season.remove();
    const c = await db.episode.remove();
    let series;
    let createSeason;
    let createEpisode;
    let seriesModel = {};
    let webSeries = await service.get();
    if (webSeries && webSeries.length != 0) {
      for (let item of webSeries) {
        if (item) {
          seriesModel.name = item._id;
          if (item.seasons && item.seasons.length != 0) {
            let season = item.seasons;
            // for (let i = 0; i <= 1; i++) {
            // if (season[i]) {
            seriesModel.image = {
              url: season[0].ImageUrlHorizontal,
              thumbnail: season[0].ImageUrlHorizontal,
              resize_url: season[0].ImageUrlVertical,
              resize_thumbnail: season[0].ImageUrlVertical,
            };
            // }
            // }
          }
          seriesModel.description = "";
          seriesModel.isWebSeries = 1;
          series = await new db.series(seriesModel).save();
          if (series) {
            let model = {};
            let uniqueSeasons = item.seasons
              .map((item) => item.pathName)
              .filter((value, index, self) => self.indexOf(value) === index);
            console.log("nbbh", uniqueSeasons);
            if (uniqueSeasons && uniqueSeasons.length != 0) {
              for (let _season of uniqueSeasons) {
                if (_season) {
                  model = {
                    parent_id: series.id,
                    name: _season,
                  };
                  createSeason = await seasonService.create(model);
                  if (createSeason) {
                    let episodeModel = {};
                    for (let episode_season of item.seasons) {
                      if (episode_season) {
                        if (episode_season.pathName == _season) {
                          episodeModel = {
                            parent_id: createSeason.id,
                            name: episode_season.latest,
                            image: {
                              url: episode_season.ImageUrlHorizontal,
                              thumbnail: episode_season.ImageUrlHorizontal,
                              resize_url: episode_season.ImageUrlVertical,
                              resize_thumbnail: episode_season.ImageUrlVertical,
                            },
                            runtime: "",
                            quality: "720 HD",
                            description: "",
                            release: "",
                            videoUrl: episode_season.videoUrl,
                            videoUrlSecond: episode_season.videoUrlSecond,
                            downloadUrl: episode_season.downloadUrlOne,
                            downloadUrlSecond: episode_season.downloadUrlSecond,
                            rating: episode_season.rating,
                          };
                          createEpisode = await episodeService.create(
                            episodeModel
                          );
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }

    // log.end();

    return series;
  } catch (err) {
    // log.end();
    throw new Error(err);
  }
};
const getById = async (req, id, context) => {
  const log = context.logger.start(`services/series/get:${id}`);
  try {
    // let series = await db.series.aggregate([
    //     { "$match": { "_id": { "$in": id } } }
    // ]);
    // let series = await db.series.aggregate([
    //   { $match: { _id: id } },
    //   {
    //     $lookup: {
    //       from: "season",
    //       localField: "_id",
    //       foreignField: "parent_id",
    //       as: "seasons",
    //     },
    //   },
    // ]);

    let seasons = [];
    let series = [];
    let _series = await db.series.findById(id);
    if (_series) {
      series = {
        name: _series.name,
        image: _series.image,
        seasons: [],
      };
      let req = { query: { parent_id: id } };
      let _seasons = await seasonService.get(req, context);
      // if category having subCategory
      if (_seasons.length != 0) {
        for (const season of _seasons) {
          if (season) {
            seasons.push({
              _id: season.id,
              name: season.name,
              episodes: [],
            });
          }
        }
      }
      for (let i = 0; i < seasons.length; i++) {
        let req = { query: { parent_id: seasons[i]._id } };
        let _episodes = await episodeService.get(req, context);
        // if category having subCategory
        if (_episodes.length != 0) {
          for (const episode of _episodes) {
            if (episode) {
              seasons[i].episodes.push({
                _id: episode.id,
                name: episode.name,
                image: episode.image,
                quality: episode.quality,
                release: episode.release,
                runtime: episode.runtime,
                videoUrl: episode.videoUrl,
                videoUrlSecond: episode.videoUrlSecond,
                downloadUrl: episode.downloadUrl,
                downloadUrlSecond: episode.downloadUrlSecond,
                rating: episode.rating,
              });
            }
          }
        }
      }

      series.seasons = seasons;
    }
    log.end();
    return series;
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};
const get = async (req, page, context) => {
  const log = context.logger.start(`services/episode/get`);
  try {
    let series;
    if (page != null && page != undefined && page != "") {
      series = await db.series
        .find({})
        .skip(page.skipCount)
        .limit(page.items)
        .sort({
          timeStamp: -1,
        });
    } else {
      series = await db.series.find({}).sort({
        timeStamp: -1,
      });
    }
    log.end();
    return series;
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};
const update = async (id, model, context) => {
  const log = context.logger.start(`services/series:${id}`);
  try {
    const entity = await db.category.findById(id);
    if (!entity) {
      throw new Error("Category not found");
    }

    const category = await set(model, entity, context);
    log.end();
    return category.save();
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};

const deleteCategories = async (id, context, res) => {
  const log = context.logger.start(`services/series/delete:${id}`);
  try {
    let categories = await db.category.findByIdAndRemove(id);

    log.end();
    return categories;
  } catch (err) {
    log.end();
    throw new Error(err);
  }
};
exports.create = create;
exports.getById = getById;
exports.get = get;
exports.update = update;
exports.deleteCategories = deleteCategories;
