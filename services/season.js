'use strict'



const set = async (entity, model, context) => {
    const log = context.logger.start('services/outlets/set')
    try {
        if (model.name) {
            entity.name = model.name
        }
        if (model.isApproved) {
            entity.isApproved = model.isApproved
        }
        if (model.isServicable) {
            entity.isServicable = model.isServicable
        }
        if (model.isPureVeg) {
            entity.isPureVeg = model.isPureVeg
        }
        if (model.averageTime) {
            entity.averageTime = model.averageTime
        }
        if (model.averagePrice) {
            entity.averagePrice = model.averagePrice
        }
        if (model.averageRating) {
            entity.averageRating = model.averageRating
        }
        if (model.image) {
            if (model.image.resize_url) {
                entity.image.resize_url = model.image.resize_url
            }
            if (model.image.resize_thumbnail) {
                entity.image.resize_thumbnail = model.image.resize_thumbnail
            }
        }
        if (model.address) {
            if (model.address.fullAddress) {
                entity.address.fullAddress = model.address.fullAddress
            }
            if (model.address.landMark) {
                entity.address.landMark = model.address.landMark
            }
            if (model.address.city) {
                entity.address.city = model.address.city
            }
            if (model.address.district) {
                entity.address.district = model.address.district
            }
            if (model.address.state) {
                entity.address.state = model.address.state
            }
            if (model.address.pinCode) {
                entity.address.pinCode = model.address.pinCode
            }
            if (model.address.country) {
                entity.address.country = model.address.country
            }
            if (model.address.location) {
                if (model.address.location.longitude) {
                    entity.address.location.longitude = model.address.location.longitude
                }
                if (model.address.location.latitude) {
                    entity.address.location.latitude = model.address.location.latitude
                }
            }
        }
        log.end()
        return entity

    } catch (err) {
        throw new Error(err)
    }
}

const create = async (body) => {
    // const log = context.logger.start(`services/season`)
    try {
        let season;
        let series;
        if (body && ((body.parent_id !== null) && (body.parent_id != undefined) && (body.parent_id != ''))) {
            try {
                series = await db.series.findById(body.parent_id);
                if (series) {
                    season = await new db.season(body).save()
                } else {
                    throw new Error('No Series Found')
                }
            } catch (error) {
                throw new Error('Invalid parent_id')
            }
        }
        // log.end()


        return season
    } catch (err) {
        // log.end()
        throw new Error(err)
    }
}
const getById = async (id, context) => {
    const log = context.logger.start(`services/outlets/get:${id}`)
    try {
        let outlet = await db.outlet.findById(id)
        log.end()
        return outlet
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}
const get = async (req, context) => {
    const log = context.logger.start(`services/season/get`)
    try {
        let seasons
        let queryModel;
        let params = req.query

        // get outlets according to outletCode
        if (params && (params.parent_id != null || params.parent_id != undefined)) {
            queryModel = {
                'parent_id': {
                    $eq: params.parent_id
                }
            }
        }

        
        seasons = await db.season.find(queryModel);

        if (seasons && seasons.length != 0) {
            //     for (let season of seasons) {
            //         if (season) {

            //             if (tempOutlets) {
            //                 for (const tempOutlet of tempOutlets) {
            //                     if (tempOutlet) {
            //                         temp.push({
            //                             _id: tempOutlet.id
            //                         })
            //                     }
            //                 }
            //             }

            //         }
            //         outlet.outlets = temp
            //         temp = []
            //     }

        }



        log.end()
        return seasons
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}
const update = async (id, model, context) => {
    const log = context.logger.start(`services/outlets:${id}`)
    try {

        const entity = await db.outlet.findById(id)
        if (!entity) {
            throw new Error('outlet not found')
        }
        let outlet = await set(entity, model, context)
        log.end()
        return outlet.save()

    } catch (err) {
        log.end()
        throw new Error(err)
    }
}


const search = async (req, context) => {
    const log = context.logger.start(`services/outlets/search`)
    try {
        let outlets
        const params = req.query;
        let queryModel = {}
        let pageNo = Number(params.pageNo) || 1
        let items = Number(params.items) || 10
        let skipCount = items * (pageNo - 1)
        let name = (params.name).toLowerCase()

        if (params && (params.name != undefined && params.name != null) && (params.pageNo != undefined && params.pageNo != null) && (params.items != undefined && params.items != null)) {
            queryModel = {
                $regex: name,
                $options: 'i'
            }

            outlets = db.outlet.aggregate(
                [{
                    $match: {
                        name: queryModel
                    }
                }]

            ).skip(skipCount).limit(items).sort({
                timeStamp: -1
            });
            // if (!outlets) {
            //     throw new Error("resturent  not found")
            // }


        } else {
            outlets = db.item.aggregate(
                [{
                    $match: {
                        name: queryModel
                    }
                }]

            )
            // if (!outlets) {
            //     throw new Error("dish not found")
            // }
        }
        log.end()
        return outlets
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

exports.create = create
exports.search = search
exports.getById = getById
exports.get = get
exports.update = update