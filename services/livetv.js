
'use strict'

const create = async (model, context) => {
    const log = context.logger.start('services/livetv/create')
    try {

        let livetv = await new db.livetv(model).save()
        log.end()
        return livetv

    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

const getById = async (id, context) => {
    const log = context.logger.start(`services/livetv/get:${id}`)

    try {
        const livetv = await db.livetv.findById(id)
        log.end()
        return livetv
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

const get = async (req, context) => {
    const log = context.logger.start(`api/livetv/get`)
    try {
        let livetv;
        livetv = await db.livetv.find({}).sort({
            timeStamp: -1
        })

        log.end()
        return livetv

    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

exports.create = create
exports.getById = getById
exports.get = get