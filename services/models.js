
'use strict'

const create = async (model, context) => {
    const log = context.logger.start('services/models/create')
    try {

        let models = await new db.models(model).save();
        log.end()
        return models

    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

const getById = async (id, context) => {
    const log = context.logger.start(`services/models/get:${id}`)

    try {
        const models = await db.models.findById(id)
        log.end()
        return models
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

const get = async (req, page, context) => {
    const log = context.logger.start(`api/models/get`)
    try {
        let models;
        models = await db.models.find({}).sort({
            title: 1
        }).skip(page.skipCount).limit(page.items);

        let totalCount = await db.models.count();
        let totalPages = totalCount / page.items;
        var p = totalPages.toString().split('.');
        totalPages = Math.floor(totalPages);
        if (p[1]) {
            totalPages++;
        };
        var result = {
            models: models,
            totalCount: totalCount,
            totalPages: totalPages,
            // category: category
        };

        log.end()
        return result

    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

exports.create = create
exports.getById = getById
exports.get = get