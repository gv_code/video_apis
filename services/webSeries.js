"use strict";
const axios = require('axios');
const service = require('../services/series')

var schedule = require('node-schedule');

// var j = schedule.scheduleJob('11 23 * * *', async () => {
//     await createMovie()
//     await createWebseries();
//     await service.create()
// });
const create = async (body, context) => {
  
}
const createWebseries = async () => {
  // const log = context.logger.start(`services/webSeries`);

  try {
    const movies = await db.webseries.remove();
    
  } catch (err) {

    throw new Error(err);
  }
  try {
    let config = {
      responseType: 'json',
      headers: { 'Content-Type': 'application/json' },
    }
    let item;
    let data = await axios.get("https://winktechsolution.info/PhpFile/webSeries.php", config);
    let res = data.data
    for (var i = 741; i < res.webSeriesList.length; i++) {
      let body = res.webSeriesList[i]
      if (body) {
        item = await new db.webseries(body).save();
        console.log(i + ' == done')
      }
    }

    // log.end();
    return item;

  } catch (err) {
    // log.end();
    throw new Error(err);
  }
};

const createMovie = async () => {
  // const log = context.logger.start(`services/createMovie`);
  try {
    const movies = await db.movies.remove();

  } catch (err) {

    throw new Error(err);
  }
  try {
    let config = {
      responseType: 'json',
      headers: { 'Content-Type': 'application/json' },
    }
    let item;
    let url = "https://winktechsolution.info/PhpFile/latest.php";
    let data = await axios.get(url, config);
    let res = data.data;

    if (res.latestList && res.latestList.length) {
      for (var i = res.latestList.length - 1; i > 0; i--) {
        let body = {
          "name": res.latestList[i].movieName,
          "description": "string",
          "category": res.latestList[i].catergory,
          "industry": res.latestList[i].Industry,
          "quality": res.latestList[i].rating,
          "imageUrl": [
            {
              "url": res.latestList[i].ImageUrlHorizontal,
              "thumbnail": res.latestList[i].ImageUrlHorizontal,
              "resize_url": res.latestList[i].ImageUrlVertical,
              "resize_thumbnail": res.latestList[i].ImageUrlVertical
            }
          ],
          "movieUrl": [
            {
              "url": res.latestList[i].videoUrl,
              "url2": res.latestList[i].videoUrlSecond
            }
          ]
        }
        if (body) {
          item = await new db.movies(body).save();
          console.log(i + ' == done')
        }
      }
    }
    // else if (res.movieList && res.movieList.length) {
    //   for (var i = res.movieList.length-1; i > 0; i--) {
    //     let body = {
    //       "name": res.movieList[i].movieName,
    //       "description": "string",
    //       "category": res.movieList[i].catergory,
    //       "industry": res.movieList[i].Industry,
    //       "quality": res.movieList[i].rating,
    //       "imageUrl": [
    //         {
    //           "url": res.movieList[i].ImageUrlHorizontal,
    //           "thumbnail": res.movieList[i].ImageUrlHorizontal,
    //           "resize_url": res.movieList[i].ImageUrlVertical,
    //           "resize_thumbnail": res.movieList[i].ImageUrlVertical
    //         }
    //       ],
    //       "movieUrl": [
    //         {
    //           "url": res.movieList[i].videoUrl,
    //           "url2": res.movieList[i].videoUrlSecond
    //         }
    //       ]
    //     }
    //     if (body) {
    //       item = await new db.movies(body).save();
    //       console.log(i+' == done')
    //     }
    //   }
    // }



    // log.end();
    return item;

  } catch (err) {
    // log.end();
    throw new Error(err);
  }
};
const get = async () => {
  // const log = context.logger.start(`api/webSeries/get`);

  try {
    let webseries = await db.webseries.aggregate([
      {
        $group: {
          _id: "$keyName",
          seasons: { $push: "$$ROOT" },
        },
      },
    ]);
    // let webseries = await db.webseries.aggregate([
    //   {
    //     $group: {
    //       _id: "$keyName",
    //       // data: { $push: "$$ROOT" },
    //     },
    //   },
    //   {
    //     $group: {
    //       _id: {
    //         series: "$_id",
    //         name: "$pathName",
    //       },
    //       data: { $push: "$$ROOT" },
    //     },
    //   },
    //   {
    //     $group: {
    //       _id: "$_id.series",
    //       seasons: { $push: { name: "$_id.name", episodes: "$data" } },
    //     },
    //   },
    // ]);

    // let webseries = await db.webseries.aggregate([
    //   {
    //     $group: {
    //       _id: {
    //         series: "$keyName",
    //         name: "$pathName",
    //         image: {
    //           url: "$ImageUrlHorizontal",
    //           thumbnail: "$ImageUrlHorizontal",
    //           resize_url: "$ImageUrlVertical",
    //           resize_thumbnail: "$ImageUrlVertical",
    //         },
    //       },
    //       episodes: {
    //         $push: {
    //           name: "$latest",
    //           rating: "$rating",
    //           image: {
    //             url: "$ImageUrlHorizontal",
    //             thumbnail: "$ImageUrlHorizontal",
    //             resize_url: "$ImageUrlVertical",
    //             resize_thumbnail: "$ImageUrlVertical",
    //           },
    //           videoUrl: "$videoUrl",
    //           downloadUrl: "$downloadUrlOne",
    //           industry: "$Industry",
    //         },
    //         // "$$ROOT"
    //       },
    //     },
    //   },
    //   {
    //     $group: {
    //       _id: {
    //         series:"$_id.series",
    //         name: "$_id.name",
    //         image:"$_id.image",
    //       },
    //       seasons: { $push: { name: "$_id.name", episodes: "$episodes" } },
    //     },
    //   },
    //   {
    //     $group: {
    //       _id: {
    //         name: "$_id.series",
    //         image: "$_id.image",
    //       },
    //       seasons: { $push: { seasons: "$seasons" } },
    //     },
    //   },
    //   { $limit: 2},
    // ]);

    // log.end();
    return webseries;
  } catch (err) {
    // log.end();
    throw new Error(err);
  }
};
exports.create = create;
exports.get = get;
exports.createMovie = createMovie;
