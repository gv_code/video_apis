
'use strict'
const puppeteer = require('puppeteer');
// const useProxy = require('puppeteer-page-proxy');
var browser = null;
var page = null;
// var puppeteer;

const close = async (model, context) => {
    try {
        if (browser) {
            await browser.close();
        }

        browser = null;
        page = null;


    } catch (err) {
        console.log("close in get error", err);
        throw new Error(err)
    }
}

const getPage = async () => {

    try {

        // if (page) {
        //     page.close()
        // }
        var b = await getBrowser();
        page = await b.newPage();
        // await useProxy(page, 'http://13.233.136.21:3128');
        return page
    } catch (err) {
        console.log("page in get error", err);

        // log.end()
        throw new Error(err)
    }
}
const getNewPage = async () => {

    try {

        var b = await getBrowser();
        var npage = await b.newPage();

        return npage
    } catch (err) {
        console.log("page in get error", err);

        // log.end()
        throw new Error(err)
    }
}

const getBrowser = async () => {
    try {
        if (!browser) {
            // args:['--proxy-server=http://13.233.136.21:3128']
            // browser = await puppeteer.launch({ headless: false,args:['--proxy-server=http://13.233.136.21:3128'] });
            browser = await puppeteer.launch();
            // browser = await puppeteer.launch({ executablePath: '/usr/bin/chromium-browser' });
        }
        return browser


    } catch (err) {
        // log.end()
        console.log("browser in get error", err);

        throw new Error(err)
    }
}

exports.close = close
exports.getPage = getPage
exports.getBrowser = getBrowser
exports.getNewPage = getNewPage