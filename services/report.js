'use strict'
const nodemailer = require('nodemailer');
// const response = require('../exchange/response')



// send mail method
const sendMail = async (email, transporter, subject, text, html) => {
    const details = {
        from: '94161asd114@gmail.com',
        to: email, // Receiver's email id
        subject: subject,
        text: text,
        html: html
    };

    var info = await transporter.sendMail(details);
    console.log("INFO:::", info)
}


// sendOtp method
const sendEmail = async (model, report, context) => {
    const email = "gvkundalwal@gmail.com"
    // transporter
    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: '94161asd114@gmail.com',
            pass: 'g123456789v'
        }
    });

    const text = model.videoId + " " + model.type;
    const subject = "Kuki Reported"

    // call sendMail method
    sendMail(email, transporter, subject, model.videoId, text)
}



const create = async (model, context) => {
    const log = context.logger.start('services/report')

    try {
        let report;
        if (model.videoId && model.type) {
            //find report 
            report = await db.report.findOne({
                'videoId': {
                    $eq: model.videoId
                }
            })
            if (!report) {

                report = await new db.report(model).save()
                // call sendOtp method
                await sendEmail(model, report, context)
                report.save();

            } else {
                log.end()
                throw new Error('Repprt already exist')
            }
            log.end()
            return report
        }

    } catch (err) {
        log.end()
        throw new Error(err)
    }

}



const getById = async (id, context) => {
    const log = context.logger.start(`services/report/getById:${id}`)

    try {
        const report = id === 'my' ? context.user : await db.report.findById(id)
        log.end()
        return report

    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

const get = async (context) => {
    const log = context.logger.start(`api/report/get`)

    try {
        let data = []
        let report

        report = await db.report.find({}).sort({
            timeStamp: -1
        })

        log.end()
        return report

    } catch (err) {
        log.end()
        throw new Error(err)
    }
}



const deleteReport = async (id, context, res) => {
    const log = context.logger.start(`services/report/delete:${id}`)
    try {
        let report = await db.report.findByIdAndRemove(id)

        log.end()
        return report
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}



exports.create = create
exports.getById = getById
exports.get = get
exports.deleteReport = deleteReport















// exports.sendSms = sendSms
// exports.verifySms = verifySms




// const smsOtp = async (url, apikey, sender, subject, otp, phone) => {
//     let data;
//     data = 'apikey=' + apikey + '&sender=' + sender + '&numbers=' + phone + '&message=' + subject + otp;
//     // const data = JSON.stringify({})
//     https.post(`https://api.textlocal.in/send?${data}`, {}).then(response => {
//             console.log(response);
//         })
//         .catch(error => {
//             console.log(error);
//         });
// }

// const sendSms = async (req, model, context) => {
//     try {
//         const params = req.query;
//         let user;
//         if (params && (params.phone != undefined && params.phone != null)) {
//             user = await db.user.findOne({
//                 'phone': {
//                     $eq: params.phone
//                 }
//             })
//         }
//         if (user == undefined) {
//             throw new Error('User not found');
//         }
//         console.log("user", user)
//         // send message
//         if (user) {
//             // generate otp 
//             const otp = randomize('0', 6)
//             user.otp = otp;
//             // user.save();
//             const subject = "The verification code for Farmer's Hut is: "
//             // const text = "you are reseiving this because you have requested the reset of the password for your account"
//             smsOtp(url, apikey, sender, subject, otp, params.phone)
//         }

//         const date = new Date();
//         const expiryTime = moment(date.setMinutes(date.getMinutes() + 3));
//         user.expiryTime = expiryTime
//         // user.save();

//         const tempToken = auth.getToken(user._id, false, context)
//         if (!tempToken) {
//             throw new Error("token error")
//         }
//         user.tempToken = tempToken

//         return user.save();
//     } catch (err) {
//         console.log('unsuccessful');
//         throw new Error(err)
//     }
// }

// const verifySms = async (model, context) => {
//     try {
//         const query = {}
//         if (!model.tempToken) {
//             throw new Error("temp token required")
//         }
//         query.tempToken = model.tempToken
//         const user = await db.user.findOne({
//             'tempToken': {
//                 $eq: query.tempToken
//             }
//         })
//         if (!user) {
//             throw new Error('user not found')
//         }

//         const a = moment(new Date()).format();
//         const mom = moment(user.expiryTime).subtract(3, 'minutes').format();
//         const isExpired = moment(a).isBetween(mom, user.expiryTime)
//         if (!isExpired) {
//             throw new Error('otp expired')
//         }


//         // Check Otp entred
//         if (model.otp != user.otp) {
//             throw new Error("otp do not match")
//         }

//         // update password
//         user.password = encrypt.getHash(model.newPassword, context)
//         // user.otp = '',
//         // user.tempToken = ''
//         user.save();
//         user.otp = '',
//             user.tempToken = ''

//         return user;
//     } catch (err) {
//         console.log('unsuccessful');
//         throw new Error(err)
//     }
// }