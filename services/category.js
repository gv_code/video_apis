
'use strict'

const create = async (model, context) => {
    const log = context.logger.start('services/category/create')
    try {

        let category = await new db.category(model).save();
        log.end()
        return category

    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

const getById = async (id, context) => {
    const log = context.logger.start(`services/category/get:${id}`)

    try {
        const category = await db.category.findById(id)
        log.end()
        return category
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

const get = async (req, context) => {
    const log = context.logger.start(`api/category/get`)
    try {
        let category;
        category = await db.category.find({}).sort({
            hTitle: 1
        })

        log.end()
        return category

    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

exports.create = create
exports.getById = getById
exports.get = get