
'use strict'

const set = async (entity, model, context) => {
    const log = context.logger.start('services/items/set')
    try {
        if (model.title) {
            entity.title = model.title
        }
        if (model.isImportant) {
            entity.isImportant = model.isImportant
        }
        if (model.message) {
            entity.message = model.message
        }
        if (model.version) {
            entity.version = model.version
        }
        if (model.link) {
            entity.link = model.link
        }
        if (model.forApproval) {
            entity.forApproval = model.forApproval
        }
        if (model.forApproval1) {
            entity.forApproval1 = model.forApproval1
        }

        log.end()
        return entity

    } catch (err) {
        throw new Error(err)
    }
}


const create = async (model, context) => {
    const log = context.logger.start('services/update/create')
    try {

        let _update = await db.update.find({}).sort({
            timeStamp: -1
        });
        if (_update.length != 0) {
            let id = _update[0].id
            const entity = await db.update.findById(id)
            if (!entity) {
                throw new Error('item not found')
            } else {
                let item = await set(entity, model, context)
                log.end()
                return item.save()
            }
        } else {
            let data = await new db.update(model).save()
            log.end()
            return data
        }
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

const getById = async (id, context) => {
    const log = context.logger.start(`services/update/get:${id}`)

    try {
        const movies = await db.update.findById(id)
        log.end()
        return movies
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

const get = async (req, context) => {
    const log = context.logger.start(`api/update/get`)
    try {
        let params = req.query
        let update;

        let _dbData = await db.update.find({}).sort({
            timeStamp: -1
        })
        let dbData = _dbData[0]
        if (!dbData) {
            throw new Error('item not found')
        } else {
            if (parseFloat(dbData.version) > parseFloat(params.ver)) {
                update = {
                    update: dbData,
                    isUpdate: true
                }
            } else {
                update = {
                    update: dbData,
                    isUpdate: false
                }
            }
        }
        log.end()
        return update

    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

exports.create = create
exports.getById = getById
exports.get = get