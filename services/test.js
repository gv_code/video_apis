
'use strict'
const axios = require("axios");
const cheerio = require('cheerio')
// const iconv = require('iconv-lite');
// const puppeteer = require('puppeteer');
var schedule = require('node-schedule');
const browser = require('./browser');
const videoService = require("./videos");

// var j = schedule.scheduleJob('23 23 * * 5', () => {
//     deleteAll({}, {})
//     strart()
// });
var cookies;
const strart = async (req, context) => {
    let query = req.query;
    let count = parseInt(query.count);
    // let _url = `https://vtrahe.tube/latest-updates/page/`;

    // for (let i = 1; i < 4; i++) {
    //     console.log(i)
    //     let url = _url + i + '/'
    //     const mittoArrey = await getData(url);
    //     if (mittoArrey) {
    //         for (let a = 0; a < mittoArrey.length; a++) {
    //             // let data = await uploadStart(mittoArrey[a])
    //             let data = await create(mittoArrey[a], context)
    //             console.log(` | ${i} | ${a} done | `)
    //         }
    //     }

    // }

    let url = `https://vtrahe.tube/latest-updates/page/1/`;


    const mittoArrey = await getData(url);
    if (mittoArrey) {
        for (let a = count; a >= 0; a--) {
            // let data = await uploadStart(mittoArrey[a])
            let data = await create(mittoArrey[a], context)
            console.log(` | ${a} done | `)
        }
    }


}

const startWithId = async (_number, context) => {
    const log = context.logger.start(`services/test/get:${url}`)
    // deleteAll({}, {})
    let url = `https://vtrahe.tube/videos/`;


    let num = parseInt(_number);
    const mittoArrey = await getData(url);
    if (mittoArrey) {
        for (let a = num; a >= 0; a--) {
            // let data = await uploadStart(mittoArrey[a])
            let data = await create(mittoArrey[a], context)
            console.log(` | ${a} done | `)
        }
    }

    log.end()
    return;
}
const uploadStart = async (data) => {


    // console.log("bodyyyyyy::::::", body);
    const res = create(data, {})
    if (res != undefined || res != null || res != "") {
        // console.log('posteddddddddddd commmmmmpppledddddddddd', res);
        return res


    }




}
const getData = async (url) => {

    let res = await get({ query: { url: url } }, {})

    if (res) {

        // console.log('class Res ::', res);
        if (res.length > 0) {
            // this.count = res.latestList.length;
            return res

        }

    }


}
const set = async (entity, model, context) => {
    const log = context.logger.start('services/items/set')
    try {
        if (model.title) {
            entity.title = model.title
        }
        if (model.isImportant) {
            entity.isImportant = model.isImportant
        }
        if (model.message) {
            entity.message = model.message
        }
        if (model.version) {
            entity.version = model.version
        }
        if (model.link) {
            entity.link = model.link
        }

        log.end()
        return entity

    } catch (err) {
        throw new Error(err)
    }
}


const create = async (model, context) => {
    // const log = context.logger.start('services/test/create')
    try {
        // let videoData = await getById(model.url, context)
        const _model = {

            title: model.title,
            dislike: model.dislike,
            like: model.like,
            duration: model.duration,
            img: model.img,
            quality: model.quality,
            url: model.url,
            description: "",

            // url: videoData.playerUrl,
            // category: videoData.category,
            // description: videoData.description,
            // downloadData: videoData.downloadData

        }
        let data = await new db.videos(_model).save();

        data = await videoService.getByIdForAuto(data._id, context);
        //    data.createdAt= new Date();
        //    data.save();
        // log.end()
        return data

    } catch (err) {
        // log.end()
        throw new Error(err)
    }
}
const deleteAll = async (context, res) => {
    // const log = context.logger.start(`services/movie/delete`);
    try {
        const movies = await db.videos.remove();

        //   log.end();
        // return movies;
    } catch (err) {
        //   log.end();
        throw new Error(err);
    }
};
const getById = async (url, context) => {
    const log = context.logger.start(`services/test/get:${url}`)
    try {



        var page = await browser.getPage();
        await page.goto(url, { waitUntil: 'networkidle2' });
        await page.waitForSelector('.jw-video');
        // if (!cookies) {
        //     const loginFormBtn = await page.$('#loginb');
        //     if (loginFormBtn) {
        //         await loginFormBtn.evaluate((e) => e.click());
        //         // page.type('#login_name', "94161asd112");
        //         // page.type('#login_password', "g11223344");
        //         await page.$eval('input[id=login_name]', el => el.value = '94161asd112');
        //         await page.$eval('input[id=login_password]', el => el.value = 'g11223344');
        //         const loginBtn = await page.$('.bpols > .subbutton');
        //         await loginBtn.evaluate((e) => e.click());
        //         await page.waitForNavigation();
        //     }

        // }

        const html = await page.content();
        let $ = cheerio.load(html);

        const name = $(' .contentbl > .fullt > h1').text();
        const _description = $('.contentbl > .vdopinf').text();
        const like = $('.contentbl > .vbuttons > #plus').text();
        const dislike = $('.contentbl > .vbuttons > #minus').text();
        // let playerUrl = $('#player > .jw-media > video').attr('src');
        // var pos = playerUrl.indexOf("/",);
        // var pos = playerUrl.split('/', 4);
        // var baseUrl = pos[0] + '//' + pos[2] + '/' + pos[3];
        // var baseUrl = playerUrl.slice(0, pos + 4)
        let playerImage = $('#player > .jw-preview').css("background-image");
        playerImage = playerImage.replace('url("', "").replace('")', "")
        // let playerUrl = "";
        const description = _description.trim()
        // let category = $('.contentbl > .vdopinf > .rpart > .catspisok > a').first()
        //     .map((i, video) => parseCategories($, video))
        //     .get();

        let cBox = $('.contentbl > .vdopinf > .rpart > .catspisok').first();

        var $c = cheerio.load(cBox.html());
        let category = $c('a').map((i, video) => parseCategories($, video))
            .get();


        cBox = $('.contentbl > .vdopinf > .rpart > .catspisok').last()
        $c = cheerio.load(cBox.html());
        let models = $c('a').map((i, video) => parseCategories($, video))
            .get();

        let tags = $('.contentbl > .vdopinf > .rpart > .tagsspisok > a')
            .map((i, video) => {
                var _tagE = $(video)
                return _tagE.text();
            })
            .get();

        let moreTag = $('.contentbl > .vdopinf > .rpart > .tagsspisok > .tagmore > a')
            .map((i, video) => {
                var _tagE = $(video)
                return _tagE.text();
            }).get();

        tags = tags.concat(moreTag)

        // let downloadData = $('.contentbl > .vbuttons > .down_block > ul > li')
        //     .map((i, video) => parsedownloadData($, video, baseUrl))
        //     .get();
        // const relatedVideos = getVideos($);

        let data = {
            name,
            playerImage,
            description,
            category,
            models,
            tags,
            like,
            dislike
        }

        log.end()
        return data


    } catch (err) {
        log.end()
        throw new Error(err)
    } finally {
        page.close();
    }

}
const time = async () => {
    await setTimeout(() => {
        return true
    }, 100)
}
const get = async (req, context) => {
    // const log = context.logger.start(`api/test/get`)
    let query = req.query;
    let siteUrl = query.url;
    try {
        // let result;
        // var response = await axios.post(siteUrl, {}, { responseType: 'arraybuffer' })
        // result = await iconv.decode(response.data, 'Windows-1251');
        var page = await browser.getPage();
        await page.goto(siteUrl, { waitUntil: 'networkidle2' });
        //  await page.waitForSelector('.pornstars');
        const html = await page.content();

        let $ = cheerio.load(html);

        const videos = await getVideos($);


        // const videos = [];



        // for (let i = 0; i < videos.length; i++) {
        //     let translate = await translatte(videos[i].title, {from: 'ru',to: 'en'});
        //     console.log("translate text",translate.text)         
        //     videos[i].title = translate.text
        // }

        // const pagination = {

        //     pages: getPages($),
        // };

        // const categories = await getModels($)
        // categories.forEach(async (e) => {
        //     var model = {
        //         title: e.title,
        //         img: e.img,
        //         url: e.url
        //     }
        //     var c = await new db.models(model).save();
        //     console.log("saved::", c)
        // });


        return videos

    } catch (err) {
        // log.end()
        throw new Error(err)
    }
}

const getVideos = ($) => {
    return $('.contentbl > #preview')
        .map((i, video) => parseVideo($, video))
        .get();
};

const getPages = ($) => {
    return $('.navigation > a')
        .map((i, page) => $(page)
            .text())
        .filter((i, page) => !isNaN(page))
        .map((i, page) => Number(page) - 1)
        .get();
};

const getModels = ($) => {
    return $('.contentbl > .maintext > .pornstars > .pornstar')
        .map((i, page) => parseModel($, page))
        .get()
};

const getCategories = ($) => {
    return $('.content > .contentbl > .categ')
        .map((i, page) => parseCategories($, page))
        .get()
};

const parseVideo = ($, video) => {
    const $video = $(video);

    const title = $video.find('.preview_title').find('a').text()
    const img = $video.find('.preview_screen').find('img').attr('src');
    const duration = $video.find('.preview_screen').find('.dlit').text();
    const like = $video.find('.preview_screen').find('.ratelike').text();
    const dislike = $video.find('.preview_screen').find('.ratedis').text();
    const quality = $video.find('.preview_screen').find('.quality').text();
    const url = $video.find('.preview_title').find('a').attr('href');


    return {
        url,
        img,
        title,
        duration,
        like,
        dislike,
        quality,
    };
};
const parseCategories = ($, video) => {
    // const $video = $(video);
    // const title = $video.find(".catname").text();
    // const count = $video.find(".catnum").text()?.split(' ')[0];
    // const img = "https://vtrahe.tube" + $video.find("img").attr('src');
    // const url = $video.find('a').attr('href');
    // return { title, url, count, img };


    const $video = $(video);
    const title = $video.text()
    const url = $video.attr('href');
    return { title, url };

};


const parseModel = ($, video) => {
    const $video = $(video);
    const title = $video.find(".pornstarname").text();
    const img = "https://vtrahe.tube" + $video.find("img").attr('src');
    const url = $video.find('a').attr('href');
    return { title, url, img };


    // const $video = $(video);
    // const title = $video.text()
    // const url = $video.attr('href');
    // return { title, url };

};

// const parsedownloadData = ($, video, baseUrl) => {
//     const $video = $(video);
//     const title = $video.find('div').text();
//     const data = $video.find('div').attr('data-c');
//     let dataArry = data.split(';');
//     var joinDUrl = dataArry[5] + '/' + dataArry[6] + '/' + dataArry[4].substring(0, 2) + '000/' + dataArry[4] + '/' + dataArry[4] + '_' + dataArry[1] + '.mp4/' + dataArry[4] + '_' + dataArry[1] + '.mp4';
//     var joinSUrl = dataArry[5] + '/' + dataArry[6] + '/' + dataArry[4].substring(0, 2) + '000/' + dataArry[4] + '/' + dataArry[4] + '_' + dataArry[1] + '.mp4'
//     var downloadUrl = "https://s" + dataArry[7] + ".stormedia.info/whlvid" + "/" + joinDUrl
//     var streemUrl = "https://s" + dataArry[7] + ".stormedia.info/whpvid" + "/" + joinSUrl
//     return {
//         title,
//         downloadUrl,
//         streemUrl
//     };
// };


const hasNextFunction = (pagination) => () => {
    const { currentPage, pages } = pagination;
    return currentPage < Math.max(...pages);
};


const hasPreviousFunction = (pagination) => () => {
    const { currentPage, pages } = pagination;
    return currentPage < Math.min(...pages);
};

const start = (req, context) => {
    const log = context.logger.start(`api/test/start`);
    // deleteAll({}, {})
    strart(req, context)

    log.end()
    return;
}

// const parsedownloadData = ($, video, baseUrl) => {
//     const $video = $(video);
//     const title = $video.find('div').text();
//     const data = $video.find('div').attr('data-c');
//     let dataArry = data.split(';');
//     var joinDUrl = dataArry[5] + '/' + dataArry[6] + '/' + dataArry[4].substring(0, 2) + '000/' + dataArry[4] + '/' + dataArry[4] + '_' + dataArry[1] + '.mp4/' + dataArry[4] + '_' + dataArry[1] + '.mp4';
//     var joinSUrl = dataArry[5] + '/' + dataArry[6] + '/' + dataArry[4].substring(0, 2) + '000/' + dataArry[4] + '/' + dataArry[4] + '_' + dataArry[1] + '.mp4'
//     var downloadUrl = "https://s" + dataArry[7] + ".stormedia.info/whlvid" + "/" + joinDUrl
//     var streemUrl = "https://s" + dataArry[7] + ".stormedia.info/whpvid" + "/" + joinSUrl
//     return {
//         title,
//         downloadUrl,
//         streemUrl
//     };
// };

const parsedownloadData = (data, data_id, baseUrl) => {
    let dataArry = data.split(';');
    var joinSUrl;
    var joinDUrl
    if (dataArry[0] == '720p') {
        joinSUrl = dataArry[4] + '/' + dataArry[5] + '/' + data_id.substring(0, 2) + '000/' + data_id + '/' + data_id  + '.mp4';
         joinDUrl = dataArry[4] + '/' + dataArry[5] + '/' + data_id.substring(0, 2) + '000/' + data_id + '/' + data_id  + '.mp4/' + data_id + '.mp4';
    }else{
         joinSUrl = dataArry[4] + '/' + dataArry[5] + '/' + data_id.substring(0, 2) + '000/' + data_id + '/' + data_id + '_' + dataArry[0] + '.mp4';
         joinDUrl = dataArry[4] + '/' + dataArry[5] + '/' + data_id.substring(0, 2) + '000/' + data_id + '/' + data_id + '_' + dataArry[0] + '.mp4/' + data_id + '_' + dataArry[0] + '.mp4';
    }
    
    var streemUrl = "https://s" + baseUrl + ".stormedia.info/whpvid" + "/" + joinSUrl
    var downloadUrl = "https://s" + baseUrl + ".stormedia.info/whlvid" + "/" + joinDUrl;
    var title = dataArry[0] == '1080p' ? 'FULL HD 1080p' : dataArry[0] == '720p' ? 'HD 720p' : dataArry[0] == '480p' ? 'SD 480p' : dataArry[0] == '360p' ? 'SD 360p' : 'LQ 240p'
    return {
        title,
        downloadUrl,
        streemUrl
    };
}


const getByIdFromOther = async (url, context) => {
    const log = context.logger.start(`services/test/get:${url}`)
    try {


        // const formdata = {
        //     reference_id: "1",
        //     uri: url,
        // };
        // const body = new URLSearchParams(formdata);
        var allhtmlv = await axios
            .get(url)
        // .then((res) =>{ 
        //     console.log(res.data)
        // });
        // let _$ = cheerio.load(allhtmlv.data);

        // // const html = await page.content();
        // const html = _$("#code").text();

        let $ = cheerio.load(allhtmlv.data);
        const like = $('.contentbl > .vbuttons > #plus').text();
        const dislike = $('.contentbl > .vbuttons > #minus').text();

        const data_c = $("#player").attr("data-q");
        const data_s = $("#player").attr("data-n");
        const data_id = $("#player").attr("data-id");
        console.log(data_id)
        const allArray = data_c.split(',');
        var downloadData = [];
        allArray.forEach((arr) => {
            var d = parsedownloadData(arr, data_id, data_s);
            downloadData.push(d)
        })
        //    var hd1080 = parsedownloadData(allArray[4], data_id, data_s,'FULL HD 1080p');
        //     var hd720 = parsedownloadData(allArray[0], data_id, data_s,'HD 720p');
        //     var hd480 = parsedownloadData(allArray[1], data_id, data_s,'SD 480p');
        //     var hd360 = parsedownloadData(allArray[2], data_id, data_s,'SD 360p');
        //     var hd240 = parsedownloadData(allArray[3], data_id, data_s,'LQ 240p');
        // https://s128.stormedia.info/whpvid/1623578082/DcIwaTHJbavWCjbVSbO1MA/17000/17430/17430_360p.mp4

        // const videoUrls = {
        //     "hd1080": hd1080,
        //     "hd240": hd240,
        //     "hd360": hd360,
        //     "hd480": hd480,
        //     "hd720": hd720
        // }

        let data = {

            downloadData,
            like,
            dislike

        }

        log.end()
        return data


    } catch (err) {
        log.end()
        throw new Error(err)
    }   

}

const getVideoThumb = async (time,id, context) => {
    const log = context.logger.start(`services/test/getvideothumb`)
    try {


        
        var data = await axios
            .get(`https://vtrahe.tube/time.php?sec=${time}&id=${id}`)
        // .then((res) =>{ 
        //     console.log(res.data)
        // });
        

        log.end()
        return data.data


    } catch (err) {
        log.end()
        throw new Error(err)
    }   

}

exports.create = create
exports.getById = getById
exports.get = get
exports.start = start
exports.startWithId = startWithId
exports.getByIdFromOther = getByIdFromOther
exports.getVideoThumb = getVideoThumb