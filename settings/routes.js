'use strict'

const fs = require('fs')
const specs = require('../specs')
const api = require('../api')
var auth = require('../permit')
const validator = require('../validators')
const fileUpload = require('express-fileupload');
const bodyParser = require('body-parser');

var multipart = require('connect-multiparty')
var multipartMiddleware = multipart()


const configure = (app, logger) => {
    const log = logger.start('settings:routes:configure')

    app.get('/specs', function (req, res) {
        fs.readFile('./public/specs.html', function (err, data) {
            if (err) {
                return res.json({
                    isSuccess: false,
                    error: err.toString()
                })
            }
            res.contentType('text/html')
            res.send(data)
        })
    })

    app.get('/api/specs', function (req, res) {
        res.contentType('application/json')
        res.send(specs.get())
    })


    // .......................users routes..............................
    // app.get('/api/users', auth.context.builder, auth.context.requiresToken, api.users.get);
    // // app.post('/api/users/verify/:id', auth.context.builder, api.users.verifyRegOtp);
    // app.post('/api/users/verifyRegOtp', auth.context.builder, auth.context.requiresToken, api.users.verifyRegOtp);
    // app.post('/api/users', auth.context.builder, validator.users.canCreate, api.users.create);

    // app.get('/api/users/:id', auth.context.builder, auth.context.requiresToken, validator.users.getById, api.users.getById);
    // app.put('/api/users/:id', auth.context.builder, auth.context.requiresToken, validator.users.update, api.users.update);
    // app.post('/api/users/login', auth.context.builder, validator.users.login, api.users.login);

    // app.post('/api/users/forgotPassword', auth.context.builder, validator.users.forgotPassword, api.users.forgotPassword);
    // app.post('/api/users/verifyOtp', auth.context.builder, auth.context.requiresToken, validator.users.verifyOtp, api.users.verifyOtp);

    // app.put('/api/users/changePassword/:id', auth.context.builder, auth.context.requiresToken, validator.users.changePassword, api.users.changePassword);
    // app.post('/api/users/logOut/:id', auth.context.builder, api.users.logOut)




    app.get('/api/users', auth.context.builder, auth.context.requiresToken, api.users.get);

    app.post('/api/users/verifyUser', auth.context.builder, validator.users.verifyUser, api.users.verifyUser);
    app.post('/api/users', auth.context.builder, validator.users.canCreate, api.users.create);

    app.get('/api/users/:id', auth.context.builder, auth.context.requiresToken, validator.users.getById, api.users.getById);
    app.put('/api/users/:id', auth.context.builder, auth.context.requiresToken, validator.users.update, api.users.update);
    app.post('/api/users/login', auth.context.builder, validator.users.login, api.users.login);

    app.post('/api/users/forgotPassword', auth.context.builder, validator.users.forgotPassword, api.users.forgotPassword);
    app.post('/api/users/resetPassword', auth.context.builder, validator.users.resetPassword, api.users.resetPassword);

    app.post('/api/users/changePassword', auth.context.builder, auth.context.requiresToken, validator.users.changePassword, api.users.changePassword);
    app.post('/api/users/logOut', auth.context.builder, auth.context.requiresToken, api.users.logOut)


    // .......................chapters routes..........................
    // app.post('/api/chapters', auth.context.builder, auth.context.requiresToken, multipartMiddleware, api.chapters.create)
    // app.get('/api/chapters/:id', auth.context.builder, auth.context.requiresToken, api.chapters.getById)
    // app.get('/api/chapters', auth.context.builder, auth.context.requiresToken, api.chapters.get)
    // app.put('/api/chapters/:id', auth.context.builder, auth.context.requiresToken, api.chapters.update)

    // ................................payments routes......................
    // app.post('/api/payments/checkout', auth.context.builder, api.payments.create)
    // app.post('/api/payments/pay', api.payments.payment)
    // app.get('/api/payments/token', auth.context.builder, auth.context.requiresToken, api.payments.paymentToken)

    // ........................quiz routes...............................
    // app.post('/api/quiz', auth.context.builder, validator.quiz.create, auth.context.requiresToken, api.quiz.create)
    // app.get('/api/quiz/:id', auth.context.builder, validator.quiz.getById, auth.context.requiresToken, api.quiz.getById)
    // app.get('/api/quiz', auth.context.builder, auth.context.requiresToken, api.quiz.get)

    // ........................result routes...................................
    // app.post('/api/results', auth.context.builder, auth.context.requiresToken, api.results.create)

    // ................................upload htmlDocuments............................................
    app.post('/api/documents', auth.context.builder, multipartMiddleware, api.documents.create)
    app.post('/api/documents/upload', multipartMiddleware, api.documents.upload);
    app.get('/api/documents', auth.context.builder, multipartMiddleware, api.documents.get)


    // ................................upload files............................................
    app.post('/api/files', auth.context.builder, multipartMiddleware, api.files.create)
    app.post('/api/files/upload', multipartMiddleware, api.files.upload);
    app.get('/api/files/:id', auth.context.builder, api.files.getById)

    // ................................ Movies ............................................
    // ................................ Movies ............................................
    app.get("/api/movies/search", auth.context.builder, api.movies.search);
    app.post(
        "/api/movies",
        auth.context.builder,
        validator.movies.create,
        api.movies.create
    );
    app.get(
        "/api/movies/:id",
        auth.context.builder,
        validator.movies.getById,
        api.movies.getById
    );
    app.get("/api/movies", auth.context.builder, api.movies.get);

    app.delete(
        "/api/movies/delete/:id",
        auth.context.builder,
        api.movies.deleteById
    );
    app.delete("/api/movies/delete", auth.context.builder, api.movies.deleteAll);


    // ............................  videos ........................

    app.post('/api/videos', auth.context.builder, api.videos.create)
    app.get('/api/videos', auth.context.builder, api.videos.get)
    app.get('/api/videos/getVideoThumb', auth.context.builder, api.videos.getVideoThumb)
    app.get('/api/videos/:id', auth.context.builder, validator.videos.getById, api.videos.getById)


    // ................................ Livetv ............................................
    app.post('/api/livetv', auth.context.builder, validator.livetv.create, api.livetv.create)
    app.get('/api/livetv/:id', auth.context.builder, validator.livetv.getById, api.livetv.getById)
    app.get('/api/livetv', auth.context.builder, api.livetv.get)

    // ................................ category ............................................
    app.post('/api/category', auth.context.builder, validator.category.create, api.category.create)
    app.get('/api/category/:id', auth.context.builder, validator.category.getById, api.category.getById)
    app.get('/api/category', auth.context.builder, api.category.get)


    // ................................ models ............................................
    app.post('/api/models', auth.context.builder, validator.models.create, api.models.create)
    app.get('/api/models/:id', auth.context.builder, validator.models.getById, api.models.getById)
    app.get('/api/models', auth.context.builder, api.models.get)

    // ................................ update ............................................
    app.post('/api/update', auth.context.builder, validator.update.create, api.update.create)
    app.get('/api/update/:id', auth.context.builder, validator.update.getById, api.update.getById)
    app.get('/api/update', auth.context.builder, api.update.get)

    app.post('/api/webSeries', auth.context.builder, api.webSeries.create)


    // ................................ test ............................................
    app.post('/api/test', auth.context.builder, api.test.create)
    app.get('/api/test/start', auth.context.builder, api.test.start)
    app.get('/api/test/:id', auth.context.builder, api.test.getById)
    app.get('/api/test', auth.context.builder, api.test.get)

    app.post('/api/zvideos', auth.context.builder, api.zvideos.create)
    app.get('/api/zvideos/start', auth.context.builder, api.zvideos.start)
    app.get('/api/zvideos/:id', auth.context.builder, api.zvideos.getById)
    app.get('/api/zvideos', auth.context.builder, api.zvideos.get)

    // ................................ report ............................................
    app.post('/api/report', auth.context.builder, validator.report.create, api.report.create)
    app.get('/api/report/:id', auth.context.builder, validator.report.getById, api.report.getById)
    app.get('/api/report', auth.context.builder, api.report.get)
    app.delete('/api/report/delete/:id', auth.context.builder, api.report.delete);

    // ...............................series routes......................................

    app.post('/api/series', auth.context.builder, validator.series.canCreate, api.series.create)
    app.get('/api/series/:id', auth.context.builder, api.series.getById)
    app.get('/api/series', auth.context.builder, api.series.get)
    app.put('/api/series/:id', auth.context.builder, api.series.update)
    app.delete('/api/series/delete/:id', api.series.delete);


    // .............................season routes..................................
    app.get('/api/season/search', auth.context.builder, api.season.search);

    app.post('/api/season', auth.context.builder, validator.season.canCreate, api.season.create);
    app.get('/api/season/:id', auth.context.builder, api.season.getById)
    app.get('/api/season', auth.context.builder, api.season.get)
    app.put('/api/season/:id', auth.context.builder, api.season.update)

    // .......................................episode routes...................................
    app.get('/api/episode/search', api.episode.search);
    app.post('/api/episode', auth.context.builder, validator.episode.canCreate, api.episode.create);
    app.get('/api/episode/:id', auth.context.builder, api.episode.getById)
    app.get('/api/episode', auth.context.builder, api.episode.get)
    app.put('/api/episode/:id', auth.context.builder, validator.season.canCreate, api.episode.update)
    app.delete('/api/episode/delete/:id', auth.context.builder, api.episode.delete);


    log.end()
}
exports.configure = configure
