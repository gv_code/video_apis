module.exports = [{
    url: '/',
    get: {
        summary: 'Search',
        description: 'get app update',
        parameters: [{
            in: 'query',
            name: 'ver',
            description: 'App version',
            type: 'integer'
        }
    ],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }

    },
    post: {
        summary: 'Create',
        description: 'Create App update',
        parameters: [
            {
                in: 'body',
                name: 'body',
                description: 'Model of App Update creation',
                required: true,
                schema: {
                    $ref: '#/definitions/updateCreateReq'
                }
            }
        ],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }
    }
},
{
    url: '/{id}',
    get:{
        summary:'Get',
        description:'get Update by Id',
        parameters:[{
            in: 'header',
            name: 'x-access-token',
            description: 'token to access api',
            required: true,
            type: 'string'
        },
        {
            in:'path',
            name:'id',
            description:'quizId',
            required:true,
            type:'string'
        }],
        responses:{
            default:{
                description:'Unexpected error',
                schema:{
                    $ref:'#/definitions/Error'
                }
            }
        }
    }
}
]