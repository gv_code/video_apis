module.exports = [
  {
    url: "/",
    get: {
      summary: "Search",
      description: "get all categories list",
      parameters: [
        {
          in: "query",
          name: "status",
          description: "active/inactive",
          required: false,
          type: "string",
        },
        {
          in: "query",
          name: "pageNo",
          description: "pageNo",
          required: false,
          type: "string",
        },
        {
          in: "query",
          name: "items",
          description: "items",
          required: false,
          type: "string",
        },
      ],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error",
          },
        },
      },
    },
    post: {
      summary: "Create",
      description: "Create Category",
      parameters: [
        {
          in: "body",
          name: "body",
          description: "Model of series creation",
          required: true,
          schema: {
            $ref: "#/definitions/seriesCreateReq",
          },
        },
      ],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error",
          },
        },
      },
    },
  },
  {
    url: "/{id}",
    get: {
      summary: "Get",
      description: "get category by Id",
      parameters: [
        {
          in: "path",
          name: "id",
          description: "categoryId",
          required: true,
          type: "string",
        },
        // {
        //     in:'query',
        //     name:'status',
        //     description:'active/inactive',
        //     required:false,
        //     type:'string'
        // }
      ],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error",
          },
        },
      },
    },
    put: {
      summary: "Update category",
      description: "update category details",
      parameters: [
        {
          in: "path",
          name: "id",
          description: "categoryId",
          required: true,
          type: "string",
        },
        {
          in: "body",
          name: "body",
          description: "Model of category update",
          required: true,
          schema: {
            $ref: "#/definitions/categoryUpdateReq",
          },
        },
      ],
    },
  },
  {
    url: "/delete/{id}",
    delete: {
      summary: "delete",
      description: " delete by Id",
      parameters: [
        {
          in: "path",
          name: "id",
          description: "categoryId",
          required: true,
          type: "string",
        },
      ],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error",
          },
        },
      },
    },
  },
];
