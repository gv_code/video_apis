module.exports = [{
        url: '/',
        get: {
            summary: 'Search',
            description: 'get all items list',
            parameters: [ {
                    in: 'query',
                    name: 'parent_id',
                    description: 'get episode by season_id',
                    required: false,
                    type: 'string'
                },
                {
                    in: 'query',
                    name: 'status',
                    description: 'active/inactive',
                    required: false,
                    type: 'string'
                },
               

            ],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        },
        post: {
            summary: 'Create',
            description: 'Create Item',
            parameters: [ {
                in: 'body',
                name: 'body',
                description: 'Model of item creation',
                required: true,
                schema: {
                    $ref: '#/definitions/episodeCreateReq'
                }
            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    }, {
        url: '/{id}',
        get: {
            summary: 'Get',
            description: 'get item by Id',
            parameters: [{
                in: 'header',
                name: 'x-access-token',
                description: 'token to access api',
                required: true,
                type: 'string'
            }, {
                in: 'path',
                name: 'id',
                description: 'itemId',
                required: true,
                type: 'string'
            }, {
                in: 'query',
                name: 'type',
                description: 'get addOns of item',
                required: false,
                type: 'string'
            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        },
        put: {
            summary: 'Update item',
            description: 'update item details',
            parameters: [ {
                in: 'path',
                name: 'id',
                description: 'itemId',
                required: true,
                type: 'string'
            }, {
                in: 'body',
                name: 'body',
                description: 'Model of item update',
                required: true,
                schema: {
                    $ref: '#/definitions/episodeUpdateReq'
                }
            }]
        }
    },

    {
        url: '/search',
        get: {
            summary: 'Search',
            description: 'get products list',
            parameters: [
                {
                    in: 'query',
                    name: 'name',
                    description: 'get dish though name',
                    required: true,
                    type: 'string'
                },
                {
                    in: 'query',
                    name: 'pageNo',
                    description: 'get dish though name',
                    required: true,
                    type: 'number'
                },
                {
                    in: 'query',
                    name: 'items',
                    description: 'get dish though name',
                    required: true,
                    type: 'number'
                }


            ],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }

    },
    {
        url: '/delete/{id}',
        delete: {
            summary: 'delete',
            description: ' delete by Id',
            parameters: [
                {
                    in: 'path',
                    name: 'id',
                    description: 'itemId',
                    required: true,
                    type: 'string'
                }
            ],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    }
]