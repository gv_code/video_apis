module.exports = [{
    url: '/',
    get: {
        summary: 'Search',
        description: 'get all season list with series id',
        parameters: [ {
            in: 'query',
            name: 'parent_id',
            description: 'get season according to series id',
            required: false,
            type: 'string'
        }
     ],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }
    },
    post: {
        summary: 'Create',
        description: 'Create Season',
        parameters: [ {
            in: 'body',
            name: 'body',
            description: 'Model of Season creation',
            required: true,
            schema: {
                $ref: '#/definitions/seasonCreateReq'
            }
        }],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }
    }
},{
    url: '/search',
    get: {
        summary: 'Search',
        description: 'get resturent',
        parameters: [{
            in: 'header',
            name: 'x-access-token',
            description: 'token to access api',
            required: true,
            type: 'string'
        },
        {
            in: 'query',
            name: 'name',
            description: 'get resturent though name',
            required: true,
            type: 'string'
        },
        {
            in: 'query',
            name: 'pageNo',
            description: 'get resturent though name',
            required: true,
            type: 'number'
        },
        {
            in: 'query',
            name: 'items',
            description: 'get resturent though name',
            required: false,
            type: 'number'
        }


        ],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }
    }

}, {
    url: '/{id}',
    get: {
        summary: 'Get',
        description: 'get outlet by Id',
        parameters: [{
            in: 'header',
            name: 'x-access-token',
            description: 'token to access api',
            required: true,
            type: 'string'
        }, {
            in: 'path',
            name: 'id',
            description: 'outletId',
            required: true,
            type: 'string'
        }],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }
    },
    put: {
        summary: 'Update outlet',
        description: 'update outlet details or add new outlets',
        parameters: [{
            in: 'header',
            name: 'x-access-token',
            description: 'token to access api',
            required: true,
            type: 'string'
        }, {
            in: 'path',
            name: 'id',
            description: 'outletId',
            required: true,
            type: 'string'
        }, {
            in: 'body',
            name: 'body',
            description: 'Model of outlet update',
            required: true,
            schema: {
                $ref: '#/definitions/outletUpdateReq'
            }
        }]
    }
}

]