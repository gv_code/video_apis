module.exports = [
  {
    url: "/",
    get: {
      summary: "Search",
      description: "get all webSeries list",
      parameters: [],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error",
          },
        },
      },
    },
  },
];
