module.exports = [{
    url: '/',
    get: {
        summary: 'Search',
        description: 'get app update',
        parameters: [{
            in: 'query',
            name: 'url',
            description: 'page url',
            type: 'string'
        }
    ],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }

    },
    post: {
        summary: 'Create',
        description: 'Create App update',
        parameters: [
            {
                in: 'body',
                name: 'body',
                description: 'Model of App Update creation',
                required: true,
                schema: {
                    $ref: '#/definitions/testCreateReq'
                }
            }
        ],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }
    }
},
{
    url: '/{id}',
    get:{
        summary:'Get',
        description:'get video by url',
        parameters:[
        {
            in:'path',
            name:'id',
            description:'video url',
            required:true,
            type:'string'
        }],
        responses:{
            default:{
                description:'Unexpected error',
                schema:{
                    $ref:'#/definitions/Error'
                }
            }
        }
    }
},
{
    url: '/start',
    get:{
        summary:'Get',
        description:'get video by url',
        parameters:[
            {
                in:'query',
                name:'count',
                description:'Start count',
                required:true,
                type:'number'
            }
        ],
        responses:{
            default:{
                description:'Unexpected error',
                schema:{
                    $ref:'#/definitions/Error'
                }
            }
        }
    }
}
]