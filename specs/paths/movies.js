module.exports = [
  {
    url: "/",
    get: {
      summary: "Search",
      description: "get quiz list",
      parameters: [],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error",
          },
        },
      },
    },
    post: {
      summary: "Create",
      description: "Create Movie",
      parameters: [
        {
          in: "body",
          name: "body",
          description: "Model of Movie creation",
          required: true,
          schema: {
            $ref: "#/definitions/moviesCreateReq",
          },
        },
      ],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error",
          },
        },
      },
    },
  },
  {
    url: "/{id}",
    get: {
      summary: "Get",
      description: "get user by Id",
      parameters: [
        {
          in: "path",
          name: "id",
          description: "quizId",
          required: true,
          type: "string",
        },
      ],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error",
          },
        },
      },
    },
  },
  {
    url: "/delete/{id}",
    delete: {
      summary: "delete",
      description: " delete by Id",
      parameters: [
        {
          in: "path",
          name: "id",
          description: "movieId",
          required: true,
          type: "string",
        },
      ],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error",
          },
        },
      },
    },
  },
  {
    url: "/delete",
    delete: {
      summary: "delete",
      description: " delete ",
      parameters: [],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error",
          },
        },
      },
    },
  },
  {
    url: "/search",
    get: {
      summary: "Search",
      description: "get movies/series list",
      parameters: [
        {
          in: "query",
          name: "name",
          description: "movieName",
          required: false,
          type: "string",
        },
        {
          in: "query",
          name: "category",
          description: "true/false",
          required: false,
          type: "string",
        },
        {
          in: "query",
          name: "categoryName",
          description: "categoryName",
          required: false,
          type: "string",
        },
        {
          in: "query",
          name: "pageNo",
          description: "pageNo",
          required: false,
          type: "string",
        },
        {
          in: "query",
          name: "items",
          description: "items",
          required: false,
          type: "string",
        },
      ],
      responses: {
        default: {
          description: "Unexpected error",
          schema: {
            $ref: "#/definitions/Error",
          },
        },
      },
    },
  },
];
