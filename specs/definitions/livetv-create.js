module.exports = {
    name: 'string',
    description: 'string',
    quality: 'string',
    language: 'string',
    imageUrl: {
        url: 'string',
        thumbnail: 'string',
        resize_url: 'string',
        resize_thumbnail: 'string'
    },
    liveUrl: {
        url: 'string'
    }

}