module.exports={
    parent_id: 'string',
    name: 'string',
    image: {
        url: 'string',
        thumbnail: 'string',
        resize_url: 'string',
        resize_thumbnail: 'string'
    },
    description: 'string',
    release: 'string',
    runtime: 'string',
    quality: 'string',
    language: 'string',
    movieUrl: {
        url: 'string',
        url2: 'string'
    },  
    isRecommanded:'string'
   
}