module.exports = {
    type: {
        type: 'string',
        enum: ['user', 'admin'],
        default: 'user'
    },
    name: 'string',
    profile:{
        image:{
            url: 'string',
            thumbnail: 'string',
            resize_url: 'string',
            resize_thumbnail: 'string'
        }
    }
    
    
    
}