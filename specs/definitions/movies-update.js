module.exports = {
    name: 'string',
    description: 'string',
    catergory: 'string',
    industry: 'string',
    release: 'string',
    runtime: 'string',
    quality: 'string',
    language: 'string',
    isRecommanded: 'string',
    imageUrl: [{
        url: 'string',
        thumbnail: 'string',
        resize_url: 'string',
        resize_thumbnail: 'string'
    }],
    movieUrl: [{
        url: 'string',
        url2: 'string'
    }]

}